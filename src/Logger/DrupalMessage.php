<?php
/**
 * @file
 *
 * Drupal message logger.
 */

namespace Drupal\PSR3\Logger;

use Drupal\PSR3\Util\Strings;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

/**
 * Class DrupalMessage
 * @package Drupal\PSR3\Logger
 *
 * PSR3 logger using Drupal's message API.
 */
final class DrupalMessage extends AbstractLogger {

  /**
   * Singleton.
   * @var self
   */
  private static $instance;

  /**
   * @return \Drupal\PSR3\Logger\Watchdog
   */
  public static function getMainLogger() {
    if (empty(self::$instance)) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   * @return null
   */
  public function log($level, $message, array $context = array()) {
    $message = preg_replace_callback('/\{(?P<keyword>[a-zA-Z0-9.-]{1,})\}/', function ($matches) use ($context) {
      if (empty($context[$matches['keyword']])) {
        return $matches['keyword'];
      }
      return Strings::mixedToString($context[$matches['keyword']]);
    }, $message);
    drupal_set_message($message, $this->translateLevel($level));
  }

  /**
   * Translate PSR3 level to Drupal message level.
   *
   * @param string $psr3Level
   * @return int
   */
  private function translateLevel($psr3Level) {
    $map = array(
      LogLevel::CRITICAL => 'error',
      LogLevel::ALERT => 'error',
      LogLevel::DEBUG => 'status',
      LogLevel::EMERGENCY => 'error',
      LogLevel::ERROR => 'error',
      LogLevel::INFO => 'status',
      LogLevel::NOTICE => 'warning',
      LogLevel::WARNING => 'warning',
    );
    return array_key_exists($psr3Level, $map) ? $map[$psr3Level] : 'status';
  }

}
