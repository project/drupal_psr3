<?php
/**
 * @file
 */

namespace Drupal\PSR3\Util;

/**
 * Class Strings
 *
 * String utility functions.
 */
class Strings {

  /**
   * @param $mixed
   * @return string
   */
  public static function mixedToString($mixed) {
    if (is_object($mixed) || is_array($mixed)) {
      return var_export($mixed, TRUE);
    }
    return $mixed;
  }

}
